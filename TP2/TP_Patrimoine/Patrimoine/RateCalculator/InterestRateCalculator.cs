﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Patrimoine.LoanValues;

namespace Patrimoine.RateCalculator
{
    public class InterestRateCalculator
    {
        public float GoodRate { get; }
        public float VeryGoodRate { get; }
        public float ExcellentRate { get; }

        public InterestRateCalculator(Duration duration)
        {
            if(duration.Value < 10)
            {
                GoodRate = 0.62f;
                VeryGoodRate = 0.43f;
                ExcellentRate = 0.35f;
            }
            else if(duration.Value < 15)
            {
                GoodRate = 0.67f;
                VeryGoodRate = 0.55f;
                ExcellentRate = 0.45f;
            }
            else if(duration.Value < 20)
            {
                GoodRate = 0.85f;
                VeryGoodRate = 0.73f;
                ExcellentRate = 0.58f;
            }
            else if(duration.Value < 25)
            {
                GoodRate = 1.04f;
                VeryGoodRate = 0.91f;
                ExcellentRate = 0.73f;
            }
            else
            {
                GoodRate = 1.27f;
                VeryGoodRate = 1.15f;
                ExcellentRate = 0.89f;
            }
        }

    }
}
