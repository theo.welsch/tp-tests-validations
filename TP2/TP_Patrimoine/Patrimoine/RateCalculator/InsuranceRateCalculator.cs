﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Patrimoine.LoanValues;

namespace Patrimoine.RateCalculator
{
    public static class InsuranceRateCalculator
    {
        public static InsuranceRate CalculateInsuranceRate(Borrower borrower)
        {
            float ratio = InsuranceOperations.BASE;

            if(borrower.Sport)
            {
                ratio += InsuranceOperations.SPORT;
            }
            if(borrower.Smoke)
            {
                ratio += InsuranceOperations.SMOKE;
            }
            if(borrower.HeartDisease)
            {
                ratio += InsuranceOperations.HEART_DISEASE;
            }
            if(borrower.ComputerEng)
            {
                ratio += InsuranceOperations.COMPUTER_ENG;
            }
            if(borrower.JetPilot)
            {
                ratio += InsuranceOperations.JET_PILOT;
            }
            return new InsuranceRate(ratio);
        }


    }
}
