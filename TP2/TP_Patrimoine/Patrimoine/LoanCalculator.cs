﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Patrimoine.LoanValues;

namespace Patrimoine
{
    public class LoanCalculator
    {
        public Capital Capital { get; }
        public Duration Duration { get; }
        public InterestRate InterestRate { get; }
        public InsuranceRate InsuranceRate { get; }
        public LoanCalculator(Capital capital, Duration duration, InterestRate interestRate, InsuranceRate insuranceRate)
        {
            Capital = capital;
            Duration = duration;
            InterestRate = interestRate;
            InsuranceRate = insuranceRate;
        }

        public float CalculateMensuality()
        {
            float RatePercent = InterestRate / 100f;
            float Denominator = 1 - (float)Math.Pow(1 + RatePercent / 12, -Duration * 12);
            float value = (Capital * (RatePercent / 12)) / Denominator;
            return value;
        }

        public float CalculateMonthlyInsuranceCotisation()
        {
            float RatePercent = InsuranceRate / 100f;
            float value = Capital * RatePercent / 12;
            return value;
        }

        public float CalculateTotalMensuality()
        {
            float value = CalculateMensuality() + CalculateMonthlyInsuranceCotisation();
            return value;
        }

        public float CalculateTotalInterest()
        {
            return CalculateTotalMensuality() * Duration * 12 - Capital;
        }

        public float CalculateTotalInsurance()
        {
            return CalculateMonthlyInsuranceCotisation() * Duration * 12;
        }

        public float CalculateAmountPaidForDuration(int years)
        {
            float value = 0;
            if (years > 0 && years <= Duration)
            {
                value = CalculateTotalMensuality() * years * 12;
            }
            else
            {
                throw new ArgumentException("La durée en année donnée n'est pas valide (1<=years<=loan duration).");
            }

            return value;
        }

    }
}
