﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patrimoine.LoanValues
{
    public static class InsuranceOperations
    {
        public static readonly float BASE = 0.3f;
        public static readonly float SPORT = -0.05f;
        public static readonly float SMOKE = 0.15f;
        public static readonly float HEART_DISEASE = 0.3f;
        public static readonly float COMPUTER_ENG = -0.05f;
        public static readonly float JET_PILOT = 0.15f;
    }
}
