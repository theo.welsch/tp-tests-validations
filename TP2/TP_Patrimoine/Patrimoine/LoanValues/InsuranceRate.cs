﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patrimoine.LoanValues
{
    public class InsuranceRate
    {
        private static readonly float MIN_RATE = 0.1f;
        private static readonly float MAX_RATE = 0.9f;
        public float Rate { get; }

        public InsuranceRate(float value)
        {
            if (value < MIN_RATE)
            {
                throw new ArgumentException("Le taux est inférieur à la valeur minimale.");
            }
            if (value > MAX_RATE)
            {
                throw new ArgumentException("Le taux est supérieur à la valeur maximale.");
            }
            Rate = value;
        }

        public static implicit operator float(InsuranceRate insuranceRate) => insuranceRate.Rate;
    }
}
