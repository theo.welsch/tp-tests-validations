﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patrimoine.LoanValues
{
    public class Capital
    {
        private static readonly uint MIN_AMOUNT = 50000;

        public int Value { get; }

        public Capital(int amount)
        {
            if (amount < MIN_AMOUNT)
            {
                throw new ArgumentException("La valeur du capital est sous le seuil minimal");
            }

            Value = amount;
        }

        public static implicit operator int(Capital capital) => capital.Value;
    }
}
