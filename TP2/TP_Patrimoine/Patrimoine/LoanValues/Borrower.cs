﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patrimoine.LoanValues
{
    public class Borrower
    {
        public bool Sport { get; set; } = false;
        public bool Smoke { get; set; } = false;
        public bool HeartDisease { get; set; } = false;
        public bool ComputerEng { get; set; } = false;
        public bool JetPilot { get; set; } = false;

    }
}
