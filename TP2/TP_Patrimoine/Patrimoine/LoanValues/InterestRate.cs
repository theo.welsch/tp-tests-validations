﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patrimoine.LoanValues
{
    public class InterestRate
    {
        private static readonly float MIN_RATE = 0f;

        public float Rate { get; }

        public InterestRate(float value)
        {
            if (value < MIN_RATE)
            {
                throw new ArgumentException("Le taux d'intérêt est inférieur à la valeur minimale.");
            }
            Rate = value;
        }

        public static implicit operator float(InterestRate interestRate) => interestRate.Rate;
    }
}
