﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patrimoine.LoanValues
{
    public class Duration
    {
        private static readonly uint MIN_DURATION = 9;
        private static readonly uint MAX_DURATION = 25;
        public int Value { get; }

        public Duration(int value)
        {
            if(value < MIN_DURATION)
            {
                throw new ArgumentException("La durée est inférieure à la durée minimale.");
            }
            if(value > MAX_DURATION)
            {
                throw new ArgumentException("La durée est supérieure à la durée maximale.");
            }

            Value = value;  
        }

        public static implicit operator int(Duration duration) => duration.Value;

    }
}
