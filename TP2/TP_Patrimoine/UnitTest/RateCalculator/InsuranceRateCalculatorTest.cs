﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xunit;

using Patrimoine.LoanValues;
using Patrimoine.RateCalculator;


namespace UnitTest.RateCalculator
{
    public class InsuranceRateCalculatorTest
    {
        [Theory]
        [MemberData(nameof(InsuranceRateCalculatorTestData.BorrowerTestData), MemberType = typeof(InsuranceRateCalculatorTestData))]
        public void TestInsuranceDataValue(Borrower borrower, float expecedValue)
        {
            InsuranceRate insuranceRate = InsuranceRateCalculator.CalculateInsuranceRate(borrower);
            Assert.Equal(expecedValue, insuranceRate);
        }
    }

    
}
