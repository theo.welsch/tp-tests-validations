﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Patrimoine.LoanValues;


namespace UnitTest.RateCalculator
{
    public class InsuranceRateCalculatorTestData
    {
        public static IEnumerable<object[]> BorrowerTestData
        {
            get
            {
                yield return new object[]
                {
                    new Borrower() {
                        Sport = true,
                        ComputerEng = true
                    }, (InsuranceOperations.BASE + InsuranceOperations.SPORT + InsuranceOperations.COMPUTER_ENG)
                };
                yield return new object[]
                {
                    new Borrower()
                    {
                        Smoke = true,
                        JetPilot = true,
                        Sport = true,
                    },
                    (InsuranceOperations.BASE + InsuranceOperations.JET_PILOT + InsuranceOperations.SMOKE + InsuranceOperations.SPORT)
                };
                yield return new object[]
                {
                    new Borrower()
                    {
                        HeartDisease = true,
                        ComputerEng = true,
                    },
                    (InsuranceOperations.BASE + InsuranceOperations.HEART_DISEASE + InsuranceOperations.COMPUTER_ENG)
                };

            }
        }
    }
}
