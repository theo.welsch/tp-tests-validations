﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xunit;
using Patrimoine.LoanValues;
using Patrimoine.RateCalculator;


namespace UnitTest.RateCalculator
{
    public class InterestRateCalculatorTest
    {
        [Theory]
        [InlineData(9, 0.62f, 0.43f, 0.35f)]
        [InlineData(10, 0.67f, 0.55f, 0.45f)]
        [InlineData(15, 0.85f, 0.73f, 0.58f)]
        [InlineData(20, 1.04f, 0.91f, 0.73f)]
        [InlineData(25, 1.27f, 1.15f, 0.89f)]
        public void TestInterestRatesValues(int years, float good, float veryGood, float excellent)
        {
            Duration duration = new Duration(years);
            InterestRateCalculator interestRateCalculator = new InterestRateCalculator(duration);
            Assert.Equal(good, interestRateCalculator.GoodRate);
            Assert.Equal(veryGood, interestRateCalculator.VeryGoodRate);
            Assert.Equal(excellent, interestRateCalculator.ExcellentRate);
        }
    }
}
