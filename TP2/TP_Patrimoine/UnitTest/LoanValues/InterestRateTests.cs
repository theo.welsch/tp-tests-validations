﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xunit;

using Patrimoine.LoanValues;

namespace UnitTest.LoanValues
{
    public class InterestRateTests
    {
        private static readonly float VALID_RATE = 0.9f;
        private static readonly float INVALID_RATE = -0.5f;

        [Fact]
        public void TestInterestRateValue()
        {
            InterestRate interestRate = new(VALID_RATE);
            Assert.Equal(VALID_RATE, interestRate);
        }

        [Fact]
        public void TestInterestRateExceptions()
        {
            ArgumentException argException = Assert.Throws<ArgumentException>(() => new InterestRate(INVALID_RATE));
            Assert.Equal("Le taux d'intérêt est inférieur à la valeur minimale.", argException.Message);
        }
    }
}
