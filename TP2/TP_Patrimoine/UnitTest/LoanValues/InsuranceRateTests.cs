﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xunit;

using Patrimoine.LoanValues;

namespace UnitTest.LoanValues
{
    public class InsuranceRateTests
    {
        private static readonly float VALID_RATE = 0.5f;
        private static readonly float INVALID_RATE_MIN = 0.01f;
        private static readonly float INVALID_RATE_MAX = 1.2f;

        [Fact]
        public void TestInsuranceRateValue()
        {
            InsuranceRate insuranceRate = new(VALID_RATE);
            Assert.Equal(VALID_RATE, insuranceRate);
        }

        [Fact]
        public void TestInsuranceRateExceptions()
        {
            ArgumentException argExceptionMin = Assert.Throws<ArgumentException>(() => new InsuranceRate(INVALID_RATE_MIN));
            Assert.Equal("Le taux est inférieur à la valeur minimale.", argExceptionMin.Message);

            ArgumentException argExceptionMax = Assert.Throws<ArgumentException>(() => new InsuranceRate(INVALID_RATE_MAX));
            Assert.Equal("Le taux est supérieur à la valeur maximale.", argExceptionMax.Message);
        }
    }
}
