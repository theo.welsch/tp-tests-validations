﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

using Patrimoine.LoanValues;

namespace UnitTest.LoanValues
{
    public class CapitalTest
    {
        private static readonly int VALID_AMOUNT = 150000;
        private static readonly int INVALID_AMOUNT = 50;
        [Fact]
        public void TestCapitalAssignation()
        {
            Capital capital = new(VALID_AMOUNT);
            Assert.Equal(VALID_AMOUNT, capital);
        }

        [Fact]
        public void TestCapitalExceptions()
        {
            ArgumentException argExc = Assert.Throws<ArgumentException>(() => new Capital(INVALID_AMOUNT));
            Assert.Equal("La valeur du capital est sous le seuil minimal", argExc.Message);
        }
    }
}
