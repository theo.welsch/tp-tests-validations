﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xunit;

using Patrimoine.LoanValues;

namespace UnitTest.LoanValues
{
    public class DurationTests
    {
        private static readonly int VALID_DURATION = 12;
        private static readonly int INVALID_DURATION_MAX = 40;
        private static readonly int INVALID_DURATION_MIN = 2;

        [Fact]
        public void TestDurationValue()
        {
            Duration duration = new(VALID_DURATION);
            Assert.Equal(duration, VALID_DURATION);
        }

        [Fact]
        public void TestDurationExceptions()
        {
            ArgumentException argExceptionMax = Assert.Throws<ArgumentException>(() => new Duration(INVALID_DURATION_MAX));
            Assert.Equal("La durée est supérieure à la durée maximale.", argExceptionMax.Message);

            ArgumentException argExceptionMin = Assert.Throws<ArgumentException>(() => new Duration(INVALID_DURATION_MIN));
            Assert.Equal("La durée est inférieure à la durée minimale.", argExceptionMin.Message);
        }
    }
}
