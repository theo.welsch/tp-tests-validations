﻿using System;
using Patrimoine.LoanValues;
using Patrimoine.RateCalculator;
using Patrimoine;

namespace MyApp // Note: actual namespace depends on the project name.
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // capital de 175000€, durée de 25 ans à un bon taux, ingé informatique fumeur
            // atteint de troubles cardiaques

            Borrower borrower = new Borrower();
            borrower.Smoke = true;
            borrower.HeartDisease = true;
            borrower.ComputerEng = true;


            Capital capital = new(175000);
            Duration duration = new(25);
            InterestRateCalculator interestRateCalculator = new(duration);
            InterestRate interestRate = new(interestRateCalculator.GoodRate);
            InsuranceRate insuranceRate = InsuranceRateCalculator.CalculateInsuranceRate(borrower);

            LoanCalculator calculator = new(capital, duration, interestRate, insuranceRate);

            Console.WriteLine("Valeurs pour le premier cas : Capital de 175 000€, durée de 25 ans à un bon taux, pour un ingénieur en informatique" +
                "fumeur atteint de troubles cardiaques");
            Console.WriteLine("Question 1 : Quel est le prix global de la mensualité ?\n\t -> {0} €", calculator.CalculateTotalMensuality());
            Console.WriteLine("Question 2 : Quel est le montant de la cotisation mensuelle d'assurance ?\n\t -> {0} €", calculator.CalculateMonthlyInsuranceCotisation());
            Console.WriteLine("Question 3 : Quel est le montant total des intérêts remborsés ?\n\t -> {0} € ", calculator.CalculateTotalInterest());
            Console.WriteLine("Question 4 : Quel est le montant total de l'assurance ?\n\t -> {0} €", calculator.CalculateTotalInsurance());
            Console.WriteLine("Question 5 : Quel montant du capital a été remboursé au bout de 10 ans ?\n\t -> {0} €", calculator.CalculateAmountPaidForDuration(10));
            Console.WriteLine("\n\n");


            // capital de 200 000€, durée de 15 ans à un très bon taux, pilote de chasse sportif

            Borrower borrower2 = new();
            borrower2.JetPilot = true;
            borrower2.Sport = true;

            Capital capital2 = new(200000);
            Duration duration2 = new(15);
            InterestRateCalculator interestRateCalculator2 = new(duration2);
            InterestRate interestRate2 = new(interestRateCalculator2.VeryGoodRate);
            InsuranceRate insuranceRate2 = InsuranceRateCalculator.CalculateInsuranceRate(borrower2);

            LoanCalculator calculator2 = new(capital2, duration2, interestRate2, insuranceRate2);

            Console.WriteLine("Valeurs pour le premier cas : Capital de 200 000€, durée de 15 ans à un très bon taux, pour un pilote de chasse sportif");
            Console.WriteLine("Question 6 : Quel est le prix global de la mensualité ?\n\t -> {0} €", calculator2.CalculateTotalMensuality());
            Console.WriteLine("Question 7 : Quel est le montant de la cotisation mensuelle d'assurance ?\n\t -> {0} €", calculator2.CalculateMonthlyInsuranceCotisation());
            Console.WriteLine("Question 8 : Quel est le montant total des intérêts remborsés ?\n\t -> {0} € ", calculator2.CalculateTotalInterest());
            Console.WriteLine("Question 9 : Quel est le montant total de l'assurance ?\n\t -> {0} €", calculator2.CalculateTotalInsurance());
            Console.WriteLine("Question 10 : Quel montant du capital a été remboursé au bout de 10 ans ?\n\t -> {0} €", calculator2.CalculateAmountPaidForDuration(10));

            Console.ReadLine();
        }
    }
}