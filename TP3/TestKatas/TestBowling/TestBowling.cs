﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xunit;
using Katas.Bowling;

namespace TestKatas.TestBowling
{
    public class TestBowling
    {
        [Fact]
        public void CanHaveFullZeroGame()
        {
            BowlingGame game = new BowlingGame();
            for (int i = 0; i < 20; i++)
            {
                game.Roll(0);
            }
            Assert.Equal(0, game.Score);
        }

        [Fact]
        public void CanHaveFullOneGame()
        {
            BowlingGame game = new BowlingGame();
            for (int i = 0; i < 20; i++)
            {
                game.Roll(1);
            }
            Assert.Equal(20, game.Score);
        }

        [Fact]
        public void CanScoreSpareFollowedByAFour()
        {
            BowlingGame game = new BowlingGame();
            game.Roll(5);
            game.Roll(5);
            game.Roll(4);
            for (int i = 0; i < 17; i++)
            {
                game.Roll(0);
            }
            Assert.Equal(18, game.Score);
        }

        [Fact]
        public void DoesntDoubleScoreWhenNotASpare()
        {
            BowlingGame game = new BowlingGame();
            game.Roll(0);
            game.Roll(5);
            game.Roll(5);
            game.Roll(3);
            for (int i = 0; i < 16; i++)
            {
                game.Roll(0);
            }
            Assert.Equal(13, game.Score);
        }

        [Fact]
        public void CanScoreStrikeFollowedByTwoFours()
        {
            BowlingGame game = new BowlingGame();
            game.Roll(10, 4,4, 0,0, 0,0, 0,0, 0,0, 0,0, 0,0, 0,0, 0,0);

            Assert.Equal(26, game.Score);
        }

        [Fact]
        public void DontScoreStrikeWhenItsASpare()
        {
            BowlingGame game = new BowlingGame();
            game.Roll(0,10, 4,4, 0,0, 0,0, 0,0, 0,0, 0,0, 0,0, 0,0, 0,0);
            Assert.Equal(22, game.Score);
        }

        [Fact]
        public void CanScorePerfectGame()
        {
            BowlingGame game = new BowlingGame();
            game.Roll(10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10);
            Assert.Equal(300, game.Score);
        }

        [Fact]
        public void CantScoreANegative()
        {
            BowlingGame game = new BowlingGame();
            Exception e = Assert.Throws<ArgumentException>(() => game.Roll(-3));
            Assert.Equal("La valeur du lancé est invalide", e.Message);
        }

        [Fact]
        public void NeedToFinishGame()
        {
            BowlingGame game = new BowlingGame();
            game.Roll(10);
            Exception e = Assert.Throws<ArgumentOutOfRangeException>(() => game.Score);
            Assert.Equal("La partie n'est pas terminée", e.Message);
        }
    }
}
