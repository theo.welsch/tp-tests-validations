﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Katas.FizzBuzz
{
    public class FizzBuzz
    {
        private static readonly IRules[] RulesArray = new IRules[3] {new FizzBuzzRule(), new FizzRule(), new BuzzRule()};

        public static string Generate(FizzBuzzValue fizzBuzzValue)
        {
            string result = "";
            for (int number = 1; number < fizzBuzzValue.Number + 1; number++)
            {
                result += GenerateElement(number);
            }
            return result;
        }

        public static string GenerateElement(int number)
        {
            foreach(IRules rule in RulesArray)
            {
                if(rule.Matches(number))
                {
                    return rule.GetReplacement();
                }
                
            }
            return number.ToString();
        }

        //Ancienne méthode pour valider les anciens tests
        public static string Generate(int chosenNumber)
        {
            string result = "";
            for(int number = 1; number < chosenNumber + 1; number++)
            {
                if(number % 3 == 0)
                {
                    result += "Fizz";
                }
                else if(number % 5 == 0)
                {
                    result += "Buzz";
                }
                else
                {
                    result += number.ToString();
                }
            }
            return result;
        }
    }
}
