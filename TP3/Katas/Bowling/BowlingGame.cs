﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Katas.Bowling
{
    public class BowlingGame
    {
        private readonly int MAX_FRAMES = 10;
        private int rolls;
        private Roll[] rounds;
        public BowlingGame()
        {
            rounds = new Roll[21];
        }

        public void Roll(params int[] rolls)
        {
            foreach(int pinDowned in rolls)
                Roll(pinDowned);
        }
        public void Roll(int pinDowned)
        {
            rounds[rolls++] = pinDowned;
        }

        public int Score
        {
            get
            {
                int score = 0;
                int cursor = 0;
                try
                {

                    for (int frame = 0; frame < MAX_FRAMES; frame++)
                    {
                        if (IsStrike(cursor))
                        {
                            score += rounds[cursor] + rounds[cursor + 1] + rounds[cursor + 2];
                            cursor++;
                        }
                        else if (IsSpare(cursor))
                        {
                            score += rounds[cursor] + rounds[cursor + 1] + rounds[cursor + 2];
                            cursor += 2;
                        }
                        else
                        {
                            score += rounds[cursor] + rounds[cursor + 1];
                            cursor += 2;
                        }
                    }
                }
                catch (Exception e)
                {
                    throw new ArgumentOutOfRangeException("La partie n'est pas terminée", e);
                }
                return score;
            }

        }

        private bool IsSpare(int cursor)
        {
            return rounds[cursor] != 10 && rounds[cursor] + rounds[cursor + 1] == 10;
        }

        private bool IsStrike(int cursor)
        {
            return rounds[cursor] == 10;
        }
    }
}
